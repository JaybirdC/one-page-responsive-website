# One Page Responsive Website

A simple demonstration of a one-page responsive website built in HTML, CSS, and Javascript.

## Getting Started

This package can be run on any browser, simply download it and run the index.html file in the browser of your choice.

### Prerequisites

A functioning browser. Note that sufficiently old browsers cannot read modern CSS.

## Deployment

This system should deploy on any browser.

## Built With

* HTML
* CSS
* Javascript
* Images courtesy of Windows Spotlight.

## Versioning

I used GIT for versioning.

## Authors

* **Calvin Marusin**

## License

This package is created and owned by Calvin Marusin. It is not authorized for use by other parties.

## Acknowledgments

Windows Spotlight provides gorgeous photos.